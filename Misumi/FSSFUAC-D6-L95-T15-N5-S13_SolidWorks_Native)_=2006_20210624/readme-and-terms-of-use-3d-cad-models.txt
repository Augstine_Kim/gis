﻿
KR   PARTcommunity/PARTserver의 24.06.2021로 부터 다운로드 합니다.

       고객님,
       
       CADENAS의 3D CAD 다운로드 포털 PARTcommunity/PARTserver의 파일 리스트를 참조하시기 바랍니다:

       
	   
       SOLIDWORKS, FSSFUAC-D6-L95-T15-N5-S13, FSSFUAC_D6_L95_T15_N5_S13.sldprt

       사용 정보:

       
       첨부파일은 빠른 다운로드를 위해 압축되어 있습니다.("ZIP")
       압축을 풀기 위해, 적당한 압축해제 소프트웨어를 사용하십시오.

       압축해제를 위한 소프트웨어가 설치되어 있지 않으면 다음에서 다운로드 할 수 있습니다:
       PKZip® (http://www.pkware.com) 또는 WinZip® (http://www.winzip.com)

       반드시 https://www.cadenas.de/kr/terms-of-use-3d-cad-models 에서 이용약관을 참조하시기 바랍니다.

       시스템 이메일주소에서 자동으로 생성된 이메일입니다. 회신하지 마십시오. 질문이 있으시면 지원팀에 문의하십시오.

       감사합니다.

       Your CADENAS GmbH
       support@cadenas.de


       
       >> 3D CAD 모델용 무료 앱 <<
       
       스마트 폰 또는 테블릿 PC를 위한 모바일 용 3D CAD 모델 앱
       http://www.cadenas.de/en/app-store에서 다운로드 하십시오.




       >> PARTcommunity - 엔지니어 네크워크 및 정보 플랫폼 <<
       
       ■ 컴포넌트의 사용예 및 아이디어
       ■ 전세계 엔지니어의 정보 및 경험의 공유

       전세계 엔지니어 커뮤니티에 참여하십시오. http://www.partcommunity.com

       
       
